<?php
session_start();
if(!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = [];
}
?>

<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Georgian Bay | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">

<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>


<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/georgian-bay-description.jpg" alt="georgian bay"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Georgian Bay</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Georgian Bay is part of the southern edge of the Canadian Shield, with granite
                bedrock exposed by the glaciers at the end of the last ice age, about 11,000 years ago.  Nestled between
                the foot of the Blue Mountains and the rugged shores and crystal-clear waters of Georgian Bay, the
                picturesque Blue Mountain Village is Southern Ontario’s premiere four season destination. The Village
                offers endless possibilities for all types of vacationers. Looking for an action-packed adventure? Send
                your heart racing with zip lines, ropes courses, hiking, biking, Segway tours, and more.  For a more
                relaxing getaway, stroll the cobblestone streets, pamper yourself at the spa, or hit the beach! Join as
                we explore the activities of Blue Mountain and the natural landscapes of its surrounding areas.</p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 1</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>08:30-12:00</td>
                    <td>London-Collingwood</td>
                    <td>Travel by coach bus to Collingwood</td>
                </tr>
                <tr>
                    <td>12:15-14:00</td>
                    <td>Duncan's Cafe - Collingwood</td>
                    <td>Lunch - North American Cuisine</td>
                </tr>
                <tr>
                    <td>14:15-16:00</td>
                    <td>Blue Mountain Resort</td>
                    <td>Free time around the Blue Mountain Resort. Explore the retail locations, grab a drink, etc.</td>
                </tr>
                <tr>
                    <td>16:00-17:00</td>
                    <td>The Westin Trillium House - Blue Mountain</td>
                    <td>Hotel Check-In</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>Oliver &amp; Bonacini Café Grill - Blue Mountain</td>
                    <td>Dinner - North American Cuisine</td>
                </tr>

            </table>


            <h4 class="description-alignment">Day 2</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>09:30-13:30</td>
                    <td>Blue Mountain Resort</td>
                    <td>Choice of Activity - Segway Tour, Bike Tour, Golf, Beach <br>(Additional fees may apply)</td>
                </tr>
                <tr>
                    <td>13:30-15:30</td>
                    <td>Bullwheel Pub - Blue Mountain</td>
                    <td>Lunch - North American Cuisine</td>
                </tr>
                <tr>
                    <td>15:30-18:30</td>
                    <td>Blue Mountain Resort</td>
                    <td>Free time around the Blue Mountain Resort. Explore the retail locations, grab a drink, etc.</td>
                </tr>
                <tr>
                    <td>18:30-20:00</td>
                    <td>C&amp;A Steak Company - Blue Mountain</td>
                    <td>Dinner - North American Cuisine</td>
                </tr>
                <tr>
                    <td>20:15-23:00</td>
                    <td>Twist Martini Restaurant &amp; Lounge - Blue Mountain</td>
                    <td>Drinks and Dancing - Optional</td>
                </tr>

            </table>
            <a id="O_GB_1527512400000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>
        </div>
    </div>
</div>
</body>
</html>