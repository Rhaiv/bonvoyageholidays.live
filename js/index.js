$(document).ready(function () {
    // Connect to firebase automatically invoked
    (function () {
        var config = {
            apiKey: "AIzaSyDFYA7wvFJmYASoYn0WutqTE_pNxFzyF-U",
            authDomain: "applicationproject-401d4.firebaseapp.com",
            databaseURL: "https://applicationproject-401d4.firebaseio.com",
            projectId: "applicationproject-401d4",
            storageBucket: "applicationproject-401d4.appspot.com",
            messagingSenderId: "44501848759"
        };
        firebase.initializeApp(config);
    })();

    // Style for Cart icon / Dropdown shopping cart
    var cartBtn = $('.sh-cart-js');
    $(cartBtn).on({
        'click': function () {
            var dropdown = $('.cart-dropdown');
            var show = 'cart-dropdown-show';
            var active = 'shop-cart-button-active';

            switch (!dropdown.hasClass(show)) {
                case true:
                    dropdown.addClass(show);
                    cartBtn.addClass(active);
                    break;
                case false:
                    dropdown.removeClass(show);
                    cartBtn.removeClass(active);
                    break;
                default:
                    break;
            }
        }
    });

    // Style for textBoxes
    $('.txtBox').on({
        'focusin': function () {
            var label = $("label[for='" + $(this).attr('id') + "']");
            $(this).removeClass('txtBox-filled-style');
            label.removeClass('return-label');
            label.addClass('move-label');
        },
        'focusout': function () {
            var label = $("label[for='" + $(this).attr('id') + "']");
            if ($(this).val() && $(this).val().length > 0) {
                $(this).addClass("txtBox-filled-style");
            } else {
                label.removeClass("move-label");
                label.addClass("return-label");
            }
        }
    });

    // Style for sign-up text boxes
    $('.txtBox-signup').on({
        'focusin': function () {
            var label = $("label[for='" + $(this).attr('id') + "']");
            $(this).removeClass('txtBox-filled-style');
            label.removeClass('return-label-signup');
            label.addClass('move-label-signup');
        },
        'focusout': function () {
            var label = $("label[for='" + $(this).attr('id') + "']");
            if ($(this).val() && $(this).val().length > 0) {
                $(this).addClass("txtBox-filled-style");
            } else {
                label.removeClass("move-label-signup");
                label.addClass("return-label-signup");
            }
        }
    });


    // LOG IN
    $('.login-js').on('click', function () {
        var email = $('#txtUsername').val();
        var password = $('#txtPassword').val();

        //sign in
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
            console.log(error.message);

        });

        firebase.auth().onAuthStateChanged(function (firebaseUser) {
            if (firebaseUser) {
                console.log(firebaseUser);


                $('.logout-js').removeClass('logout-hide').addClass('logout-size');
                $('.txtBox').val("").removeClass('txtBox-filled-style');
                $('.label-style').removeClass("move-label").addClass("return-label");
            } else {
                // Not logged in
            }
        });
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
            .then(function () {
                return firebase.auth().signInWithEmailAndPassword(email, password);
            })
            .catch(function (error) {
                var errorMessage = error.message;
                console.log("This is your persistence error message: " + errorMessage);
            });
        $.post("/ajax.php?ajax=true", {
            test_data: email
        }, function (data) {
            console.log(data);
        });
    });

    // LOG OUT
    $('.logout-js').on('click', function () {
        firebase.auth().signOut().then(function () {
            //success
            $('.logout-js').addClass('logout-hide').removeClass('logout-size');

        }).catch(function (error) {
            //error
        });
    });

    // button click event listener for the sign up button
    $('.sign-up-js').on('click', function () {
        var fname, lname, email, password;
        var errors = $('#errors');
        var fname_element = $('#first_name');
        var lname_element = $('#last_name');
        var pw_element = $('#password-signup');
        var confirm_pw = $('#confirm-password');
        var email_element = $('#email');
        var atpos = email_element.val().indexOf("@");
        var dotpos = email_element.val().lastIndexOf(".");

        $('.error').remove();

        // checks if the first name field is empty
        // before assigning a value to fname
        if (fname_element.val().length > 0) {
            fname = fname_element.val();
        } else {
            errors.append("<li class='error'>Please enter your first name.</li>");
        }

        // checks if the last name field is empty
        // before assigning a value to lname
        if (lname_element.val().length > 0) {
            lname = lname_element.val();
        } else {
            errors.append("<li class='error'>Please enter  your last name.</li>");
        }

        // checks to make sure the password fields match
        // before assigning a value to password
        if (pw_element.val() !== confirm_pw.val()) {
            errors.append("<li class='error'>Passwords do not match.</li>");
        } else if (!(pw_element.val().length >= 6)) {
            errors.append("<li class='error'>Passwords must be greater than 6 characters in length.</li>")
        } else {
            password = pw_element.val();
        }

        // checks the email field with very simple email
        // address validation
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email_element.val().length) {
            errors.append("<li class='error'>Not a valid e-mail address.</li>");
        } else {
            email = email_element.val();
        }

        // Checks for errors on sign up page
        if (errors.has(".error").length) {
            // there were errors, do nothing
            // as the errors will be on screen already
        } else {
            //sign in
            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
                console.log(error.message);
            });

            firebase.auth().onAuthStateChanged(function (firebaseUser) {
                if (firebaseUser) {
                    console.log(firebaseUser);


                    // Save user data to the actual db
                    firebase.database().ref('Group_BLT/Team_Data/Users/' + firebaseUser.uid + '/profile_data').set({
                        first_name: fname,
                        last_name: lname,
                        username: email,
                        email: email
                    });
                } else {
                    console.log('not logged in');
                }

                $('.txtBox-signup').val("").removeClass('txtBox-filled-style');
                $('.label-style-signup').removeClass("move-label-signup").addClass("return-label-signup");
            });
        }
    });
    $('.btn-add-to-cart').on({
        'click': function() {
            var product = $(this).attr('id');
            var array = product.split("_");
            var province;
            var tourName;
            var hrTourName;
            switch (array[0]) {
                case 'O':
                    province = 'Ontario';
                    break;
                case 'Q':
                    province = 'Quebec';
                    break;
                case 'E':
                    province = 'EastCoast';
                    break;
                default:
                    province = null;
                    break;
            }

            switch(array[1]) {
                case 'NB':
                    tourName = 'NewBrunswick';
                    hrTourName = 'New Brunswick';
                    break;
                case 'NB-NS':
                    tourName = 'NewBrunswick-NovaScotia';
                    hrTourName = 'New Brunswick and Nova Scotia';
                    break;
                case 'NB-Q':
                    tourName = 'NewBrunswick-Quebec';
                    hrTourName = 'New Brunswick and Quebec';
                    break;
                case 'NS':
                    tourName = 'NovaScotia';
                    hrTourName = 'Nova Scotia';
                    break;
                case 'GB':
                    tourName = 'GeorgianBay';
                    hrTourName = 'Georgian Bay';
                    break;
                case 'NF':
                    tourName = 'NiagaraFalls';
                    hrTourName = 'Niagara Falls';
                    break;
                case 'NOL':
                    tourName = 'Niagara-nol';
                    hrTourName = 'Niagara on the Lake';
                    break;
                case 'OW':
                    tourName = 'Ottawa';
                    hrTourName = 'Ottawa';
                    break;
                case 'TO':
                    tourName = 'Toronto';
                    hrTourName = 'Toronto';
                    break;
                case 'TT':
                    tourName = 'Toronto-Theatre';
                    hrTourName = 'Toronto Theatre';
                    break;
                case 'MT':
                    tourName = 'MontTremblant';
                    hrTourName = 'Mont Tremblant';
                    break;
                case 'ML':
                    tourName = 'Montreal';
                    hrTourName = 'Montreal';
                    break;
                case 'QRR':
                    tourName = 'Quebec-R-R';
                    hrTourName = 'Quebec City - Rivierre-du-Loup - Rimouski';
                    break;
                case 'QC':
                    tourName = 'QuebecCity';
                    hrTourName = 'Quebec City';
                    break;
                default:
                    tourName = null;
                    break;
            }

            var tripRef = firebase.database().ref('Group_BLT/Team_Data/Products/Trips/' + province + "/" + tourName + "/" + product);
            tripRef.on("value", function(snapshot) {
                if(snapshot) {
                    console.log(snapshot);
                    var days = snapshot.child("Days").val();
                    var description = snapshot.child("Description").val();
                    var price = snapshot.child("Price").val();
                    var id = snapshot.key;
                    var cart = $('.cart-table-body');
                    var call = 'set';
                    var key = "";
                    $.post("/ajax.php?ajax=true", {
                        id: id,
                        tourName: hrTourName,
                        days: days,
                        price: price,
                        description: description,
                        phpfunc: call
                    }, function (data) {
                        key = data;
                        cart.append(
                            "<tr class='cart-item-row'>" +
                            "<td><a id='" + key + "' class='rm-cart-item' name='rm-cart-item'><i class=\"fas fa-trash-alt\"></i></a></td>" +
                            "<td>" + hrTourName + "</td>" +
                            "<td>" + "$" + price + ".00" + "</td>" +
                            "</tr>"
                        );
                    });
                }
            });
        }
    });
    $('.cart-table-body').on('click', '.rm-cart-item', function() {
        var id = $(this).attr('id');
        $.post("/ajax.php?ajax=true", {
            close_id: id
        }, function (data) {
            console.log(data);
        });
        $(this).closest('tr').remove();
    });
    
    $('.btn-cart-checkout-js').on({
        'click': function() {
            location.href = "./checkout.php";
        }
    });

    $('.btn-thank-you').on({
        'click': function() {
            location.href = "./index.php";
        }
    });
});
