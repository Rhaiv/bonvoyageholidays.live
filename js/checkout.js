$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "/ajax.php?ajax=true",
        dataType: "text",
        data: {
            checkout: "checkout"
        },
        success: function (data) {
            console.log(JSON.parse(data));

            $.each(JSON.parse(data), function () {
                $('.tbody-wrapper').append(
                    "<tr>" +
                    "<td>" + this.tourName + "</td>" +
                    "<td>" + this.description + "</td>" +
                    "<td>$" + this.price + ".00</td>" +
                    "</tr>"
                )
            });
        },
        error: function (jqXHR, status, e) {
            console.log(e);
        }
    });
    $('.btn-checkout-page').on({
        'click': function() {
            $.ajax({
                type: "POST",
                url: "ajax.php?ajax=true",
                data: {
                    final: "finalize"
                },
                success: function () {
                    location.href="./thankyou.php";
                },
                error: function () {

                }
            });

        }
    });
});