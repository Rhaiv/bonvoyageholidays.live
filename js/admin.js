$(document).ready(function () {
    var ontario = $('.Ontario');
    var quebec = $('.Quebec');
    var eastcoast = $('.EastCoast');
    var ontariorb = $('.ontariorb');
    var quebecrb = $('.quebecrb');
    var eastcoastrb = $('.eastcoastrb');


    if (!(ontariorb.is(':checked'))) {
        ontario.hide();
    }
    if (!(quebecrb.is(':checked'))) {
        quebec.hide();
    }
    if (!(eastcoastrb.is(':checked'))) {
        eastcoast.hide();
    }

    ontariorb.on({
        'click': function () {
            if (ontariorb.is(':checked')) {
                ontario.show();
                eastcoast.hide();
                quebec.hide();
            }
        }
    });

    quebecrb.on({
        'click': function () {
            if (quebecrb.is(':checked')) {
                quebec.show();
                ontario.hide();
                eastcoast.hide();
            }
        }
    });

    eastcoastrb.on({
        'click': function () {
            if (eastcoastrb.is(':checked')) {
                eastcoast.show();
                ontario.hide();
                quebec.hide();
            }
        }
    });

    $('.btn-admin-submit').on({
        'click': function () {
            var province = $('input[name=province]:checked').val();
            var tourName = $('input[name=bg]:checked').val();
            var id = [];

            switch (province) {
                case 'Ontario':
                    id[0] = 'O';
                    break;
                case 'Quebec':
                    id[0] = 'Q';
                    break;
                case 'EastCoast':
                    id[0] = 'E';
                    break;
                default:
                    id[0] = 0;
                    break;
            }

            switch (tourName) {
                case 'NewBrunswick':
                    id[1] = 'NB';
                    break;
                case 'NewBrunswick-NovaScotia':
                    id[1] = 'NB-NS';
                    break;
                case 'NewBrunswick-Quebec':
                    id[1] = 'NB-Q';
                    break;
                case 'NovaScotia':
                    id[1] = 'NS';
                    break;
                case 'GeorgianBay':
                    id[1] = 'GB';
                    break;
                case 'NiagaraFalls':
                    id[1] = 'NF';
                    break;
                case 'Niagara-nol':
                    id[1] = 'NOL';
                    break;
                case 'Ottawa':
                    id[1] = 'OW';
                    break;
                case 'Toronto':
                    id[1] = 'TO';
                    break;
                case 'Toronto-Theatre':
                    id[1] = 'TT';
                    break;
                case 'MontTremblant':
                    id[1] = 'MT';
                    break;
                case 'Montreal':
                    id[1] = 'ML';
                    break;
                case 'Quebec-R-R':
                    id[1] = 'QRR';
                    break;
                case 'QuebecCity':
                    id[1] = 'QC';
                    break;
            }

            var todate = $('.date-to').val() + "T19:00:00";
            var todateEpoch = Date.parse(todate);
            var datetime = $('.date-from').val() + "T" + $('.depart-time').val();
            var departDateEpoch = Date.parse(datetime);
            var product = id[0] + "_" + id[1] + "_" + departDateEpoch;
            var description = $('.description-field-admin').val();
            var ref = firebase.database().ref('Group_BLT/Team_Data/Products/Trips');
            var provRef = ref.child(province);
            var tourRef = provRef.child(tourName);
            var productRef = tourRef.child(product);
            var price = $('.price').val();
            var duration = Math.ceil(((todateEpoch - departDateEpoch) / 86400000));

            productRef.set({
                "Price": price,
                "Description": description,
                "Duration": duration
            });
        }
    })
});