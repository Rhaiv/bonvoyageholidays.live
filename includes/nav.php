<div class="navbar">
    <ul class="navbar-ul">
        <li><a class="nav-link-design" href="/">Home</a></li>
        <li>
            <div class="dropdown">
                <button class="dropbtn">Trips</button>
                <div class="dropdown-content">
                    <ul>
                        <li><a class="nav-link-design" href="/ontario">Ontario</a></li>
                        <li><a class="nav-link-design" href="/quebec">Quebec</a></li>
                        <li><a class="nav-link-design" href="/eastcoast">East Coast</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a class="nav-link-design" href="./#contact">Contact</a></li>
        <li><a class="nav-link-design" href="./#about">About Us</a></li>
        <li><a class="nav-link-design login" href="/login">Login</a></li>
    </ul>
    <a class="shop-cart-button sh-cart-position sh-cart-js">
        <i class="fas fa-shopping-cart sh-cart-size"></i>
    </a>
</div>