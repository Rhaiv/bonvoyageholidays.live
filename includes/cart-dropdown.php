<?php
?>
<div class="cart-dropdown" id="cart-dropdown">
    <div class="arrow-up"></div>
    <div class="cart-content">
        <p class="cart-title">Your Cart:</p>
            <table class='table-wrapper cart-table'>
                <thead>
                <tr class="header-row">
                    <th><i class="fas fa-trash-alt"></i></th>
                    <th>Trip</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody class="cart-table-body">
                <?php
                if (isset($_SESSION['cart'])) {
                    foreach ($_SESSION['cart'] as $key => $value) { ?>
                        <tr class="cart-item-row">
                            <td>
                                <?php echo "<a name='rm-cart-item' id=\"" . $key . "\" class=\"rm-cart-item\"><i class=\"fas fa-trash-alt\"></i></a>"; ?>
                            </td>
                            <td>
                                <?php echo $value['tourName']; ?>
                            </td>
                            <td>
                                <?php echo "$" . $value['price'] . ".00"; ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        <a class="btn-cart-checkout-js btn-cart-checkout">Checkout</a>
    </div>
    <!--    <div class="arrow-down"></div>-->
    <!--    <div class="arrow-left"></div>-->
    <!--    <div class="arrow-right"></div>-->
</div>
