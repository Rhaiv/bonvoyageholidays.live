<?php
session_start();
$tourName = "";
$price = "";
$description = "";
$days = "";
$id = "";

if (isset($_GET['ajax'])) {
    function add_to_cart()
    {
        $tourName = $_POST['tourName'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $days = $_POST['days'];
        $id = $_POST['id'];

        $array = [
            "id" => $id,
            "tourName" => $tourName,
            "price" => $price,
            "description" => $description,
            "days" => $days
        ];

        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
            array_push($_SESSION['cart'], $array);
        } else {
            array_push($_SESSION['cart'], $array);
        }
        $endPointer = end($_SESSION['cart']);
        $key = key($_SESSION['cart']);

        echo json_encode($key);
    }

    function rm_from_cart($id) {
        unset($_SESSION['cart'][$id]);
    }

    if (isset($_POST['phpfunc'])) {
        add_to_cart();
    }
    if (isset($_POST['close_id'])) {
        rm_from_cart($_POST['close_id']);
    }

    if(isset($_POST['checkout'])) {
        echo json_encode($_SESSION['cart']);
    }


    if (isset($_POST['final'])) {
        unset($_SESSION['cart']);
    }
}


