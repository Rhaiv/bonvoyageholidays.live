<?php
session_start();
?>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<body>
<div class="sign-up-form">
    <div class="sign-up-input-wrapper">
        <div id="error_messages">
            <ul id="errors">
            </ul>
        </div>
        <label class="label-style-signup" for="first_name">First Name</label>
        <input class="txtBox-signup" id="first_name" type="text"/>
        <label class="label-style-signup" for="last_name">Last Name</label>
        <input class="txtBox-signup" id="last_name" type="text"/>
        <label class="label-style-signup" for="email">Email</label>
        <input class="txtBox-signup" id="email" type="text"/>
        <label class="label-style-signup" for="password-signup">Password</label>
        <input class="txtBox-signup" id="password-signup" type="password"/>
        <label class="label-style-signup" for="confirm-password">Confirm Password</label>
        <input class="txtBox-signup" id="confirm-password" type="password"/>
        <div class="signup-btn-wrapper">
            <a class="login-page-button sign-up-js" name="signUp">Sign Up</a>
            <a href="/" class="login-page-button cancel-button-js">Cancel</a>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
</body>

</html>

<?php ?>