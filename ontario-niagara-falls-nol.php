<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Niagara Falls &amp; Niagara-On-The-Lake | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">

<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/nol-description.jpg" alt="niagara on the lake"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Niagara Falls &amp; Niagara-On-The-Lake</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Lorem ipsum dolor sit amet, no ipsum iudicabit pro, eu mediocrem iudicabit
                incorrupte vis, etiam consul
                eleifend te duo. Ornatus abhorreant ne sit, ei sint eruditi his, erant commune nec no. Dolore incorrupte
                sea ex, vim tempor verterem ei. Aeque possim et eum, modus cetero probatus te ius. Augue feugiat
                ullamcorper ea sit. Purto vulputate moderatius ei mea, nibh similique vulputate id eos, nibh autem
                at usu. Has ea ullum diceret, vis in malorum tincidunt.</p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 6</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>10:30-12:00</td>
                    <td>Trius Winery</td>
                    <td>VIP Tour &amp; Tasting</td>
                </tr>
                <tr>
                    <td>12:00-13:30</td>
                    <td>Trius Winery Restaurant</td>
                    <td>Lunch - Prix Fixe menu</td>
                </tr>
                <tr>
                    <td>13:45-15:00</td>
                    <td>Inniskillin Winery</td>
                    <td>Tour &amp; Wine Tasting</td>
                </tr>
                <tr>
                    <td>15:10-15:45</td>
                    <td>Reif Estate Winery</td>
                    <td>Wine Tasting</td>
                </tr>
                <tr>
                    <td>15:50-16:15</td>
                    <td>Lailey Winery</td>
                    <td>Wine Tasting</td>
                </tr>
                <tr>
                    <td>16:20-17:00</td>
                    <td>Two Sisters Vineyard</td>
                    <td>Wine Tasting</td>
                </tr>
                <tr>
                    <td>17:45-19:30</td>
                    <td>Irish Harp Pub</td>
                    <td>Dinner - Pub Fare</td>
                </tr>
            </table>

            <h4 class="description-alignment">Day 7</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>09:00-10:30</td>
                    <td>HobNob</td>
                    <td>Breakfast - American Cuisine</td>
                </tr>
                <tr>
                    <td>10:45-12:30</td>
                    <td>Peller Estates Winery</td>
                    <td>Tour &amp; Wine Tasting</td>
                </tr>
                <tr>
                    <td>13:00-16:00</td>
                    <td>Fallsview Casino</td>
                    <td>Casino Free Time</td>
                </tr>
                <tr>
                    <td>16:00-18:30</td>
                    <td>Niagara Falls-London</td>
                    <td>Travel by coach bus back to London</td>
                </tr>
            </table>
            <a id="O_NOL_1525611600000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>


        </div>
    </div>
</div>
</body>
</html>