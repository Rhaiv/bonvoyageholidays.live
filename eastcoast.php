<?php
    session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>East Coast Trips | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-lists">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<a class="login-page-button logout logout-js logout-hide">Log Out</a>
<img src="images/logo.png" alt="bon voyage holidays logo"/>
<div class="table-wrapper">
    <div class="table">
        <h2>East Coast</h2>
        <table>
            <tr>
                <th>Location(s)</th>
                <th>Length of Trip</th>
                <th>Dates</th>
            </tr>
            <tr>
                <td><a href="eastcoast-nb">New Brunswick</a></td>
                <td>10 days</td>
                <td>Monday, May 7 2018 - Wednesday, May 16 2018
                    <br>Monday, June 4 2018 - Wednesday, June 13 2018
                    <br>Tuesday, July 3 2018 - Thursday, July 12 2018
                </td>
            </tr>
            <tr>
                <td><a href="eastcoast-nb-quebec">New Brunswick and Quebec</a></td>
                <td>14 days</td>
                <td>Tuesday, May 22 2018 - Monday, June 4 2018
                    <br>Tuesday, June 12 2018 - Monday, June 25 2018
                </td>
            </tr>
            <tr>
                <td><a href="eastcoast-nb-ns">New Brunswick and Nova Scotia</a></td>
                <td>14 days</td>
                <td>Saturday, May 5 2018 - Friday, May 18 2018
                    <br>Saturday, June 10 2018 - Friday, June 23 2018
                </td>
            </tr>
            <tr>
                <td><a href="eastcoast-ns">Nova Scotia</a></td>
                <td>10 days</td>
                <td>Monday, May 7 2018 - Wednesday, May 16 2018
                    <br>Monday, June 4 2018 - Wednesday, June 13 2018
                    <br>Tuesday, July 3 2018 - Thursday, July 12 2018
                </td>
            </tr>
        </table>
    </div>
</div>

</body>
</html>