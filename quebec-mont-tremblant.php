<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mont Tremblant | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">

<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>

<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/mont-tremblant-description.jpg" alt="mont tremblant gondola"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Mont Tremblant</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Lorem ipsum dolor sit amet, no ipsum iudicabit pro, eu mediocrem iudicabit
                incorrupte vis, etiam consul
                eleifend te duo. Ornatus abhorreant ne sit, ei sint eruditi his, erant commune nec no. Dolore incorrupte
                sea ex, vim tempor verterem ei. Aeque possim et eum, modus cetero probatus te ius. Augue feugiat
                ullamcorper ea sit. Purto vulputate moderatius ei mea, nibh similique vulputate id eos, nibh autem
                at usu. Has ea ullum diceret, vis in malorum tincidunt.</p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 1</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>07:30-16:30</td>
                    <td>London-Mont-Tremblant</td>
                    <td>Travel by coach bus to Mont-Tremblant</td>
                </tr>
                <tr>
                    <td>16:30-17:30</td>
                    <td>Holiday Inn Express &amp; Suites Tremblant</td>
                    <td>Hotel Check-In</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>La Savoie</td>
                    <td>Dinner - Fondue</td>
                </tr>

            </table>


            <h4 class="description-alignment">Day 2</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>08:30-09:45</td>
                    <td>La Maison de la Crêpe</td>
                    <td>Breakfast - Crepes</td>
                </tr>
                <tr>
                    <td>10:00-13:45</td>
                    <td>Mont-Tremblant Village</td>
                    <td>Free Time</td>
                </tr>
                <tr>
                    <td>14:00-17:30</td>
                    <td>Mont-Tremblant</td>
                    <td>Choice of Activities - Zipline, Bike, Golf, Horseback Riding<br>Additional fees may apply</td>
                </tr>
                <tr>
                    <td>18:30-20:00</td>
                    <td>La Forge</td>
                    <td>Dinner - Steakhouse</td>
                </tr>
                <tr>
                    <td>20:15-23:00</td>
                    <td>Casino Mont-Tremblant</td>
                    <td>Casino Night - Optional</td>
                </tr>

            </table>
            <a id="Q_MT_1525698000000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>

        </div>
    </div>
</div>
</body>
</html>