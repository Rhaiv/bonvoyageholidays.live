<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways! Come shop with us! Book a trip today!">
    <meta name="og:title" content="Bon Voyage Holidays | Home">
    <meta name="og:description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways!">
    <meta name="og:image" content="https://www.bonvoyageholidays.live/images/logo_300x300.png">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
    <title>Home | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script defer src="js/checkout.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<?php
    echo "<pre>";
    print_r($_SESSION['shopping_cart']);
    echo "</pre>";
?>
<?php require_once("./includes/nav.php"); ?>
<h1 class="checkout-header">Checkout</h1>
<table class="table-wrapper checkout-wrapper checkout-style">
    <tbody class="tbody-wrapper">
    </tbody>
</table>
<a id="" class="btn-cart-checkout btn-checkout-page">Checkout</a>
</body>
</html>