<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quebec City - Riviere-du-Loup - Rimouski | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/qrr-description.jpg" alt="aerial view of rimouski"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Quebec City - Riviere-du-Loup - Rimouski</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Lorem ipsum dolor sit amet, no ipsum iudicabit pro, eu mediocrem iudicabit
                incorrupte vis, etiam consul
                eleifend te duo. Ornatus abhorreant ne sit, ei sint eruditi his, erant commune nec no. Dolore incorrupte
                sea ex, vim tempor verterem ei. Aeque possim et eum, modus cetero probatus te ius. Augue feugiat
                ullamcorper ea sit. Purto vulputate moderatius ei mea, nibh similique vulputate id eos, nibh autem
                at usu. Has ea ullum diceret, vis in malorum tincidunt.</p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 8</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>09:00-12:30</td>
                    <td>Old Quebec</td>
                    <td>Free Time in Old Quebec</td>
                </tr>
                <tr>
                    <td>12:45-15:30</td>
                    <td>Riviere-du-Loup</td>
                    <td>Travel by coach bus to Riviere-du-Loup</td>
                </tr>
                <tr>
                    <td>15:45-16:30</td>
                    <td>Auberge de la Pointe</td>
                    <td>Hotel Check-In</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>Restaurant Le Saint-Patrice<td>
                    <td>Dinner - French Cuisine</td>
                </tr>

            </table>

            <h4 class="description-alignment">Day 9</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>09:00-10:30</td>
                    <td>Mikes</td>
                    <td>Breakfast - North American Cuisine</td>
                </tr>
                <tr>
                    <td>10:45-14:30</td>
                    <td>Parc des Chutes</td>
                    <td>Explore the park and waterfall</td>
                </tr>
                <tr>
                    <td>14:45-16:30</td>
                    <td>Observatoire</td>
                    <td>Explore the shoreline</td>
                </tr>
                <tr>
                    <td>18:00-23:00</td>
                    <td>Alexandra Pier<td>
                    <td>Dinner & Fireworks Cruise</td>
                </tr>

            </table>
            <a id="Q_QRR_1527339600000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>

        </div>
    </div>
</div>
</body>
</html>