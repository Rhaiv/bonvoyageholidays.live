<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toronto | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">
<?php require_once("./includes/nav.php"); ?>


<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/toronto-description.jpg" alt="toronto skyline"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Toronto</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Lorem ipsum dolor sit amet, no ipsum iudicabit pro, eu mediocrem iudicabit
                incorrupte vis, etiam consul
                eleifend te duo. Ornatus abhorreant ne sit, ei sint eruditi his, erant commune nec no. Dolore incorrupte
                sea ex, vim tempor verterem ei. Aeque possim et eum, modus cetero probatus te ius. Augue feugiat
                ullamcorper ea sit. Purto vulputate moderatius ei mea, nibh similique vulputate id eos, nibh autem
                at usu. Has ea ullum diceret, vis in malorum tincidunt.</p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 1</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>09:30-11:45</td>
                    <td>London-Toronto</td>
                    <td>Travel by coach bus to Toronto</td>
                </tr>
                <tr>
                    <td>12:00-13:30</td>
                    <td>Fran's Restaurant</td>
                    <td>Lunch - Diner Fare</td>
                </tr>
                <tr>
                    <td>13:30-16:30</td>
                    <td>CF Toronto Eaton Centre</td>
                    <td>Free Time</td>
                </tr>
                <tr>
                    <td>16:30-17:30</td>
                    <td>Pantages Hotel Toronto</td>
                    <td>Hotel Check-In</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>JOEY Eaton Centre</td>
                    <td>Dinner - Canadian Cuisine</td>
                </tr>
            </table>


            <h4 class="description-alignment">Day 2</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>Morning/td>
                    <td>Downtown Toronto</td>
                    <td>Free Time</td>
                </tr>
                <tr>
                    <td>13:30-16:30</td>
                    <td>Ed Mirvish Theatre</td>
                    <td>Theatre Performance - Annie (May), Chicago (June, July)</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>Donatello Restaurant</td>
                    <td>Dinner - Italian Cuisine</td>
                </tr>

            </table>
            <a id="O_TO_1527512400000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>

        </div>
    </div>
</div>
</body>
</html>