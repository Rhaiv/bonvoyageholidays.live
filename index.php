<?php
session_start();
if (!isset($_SESSION['shopping_cart'])) {
    $_SESSION['shopping_cart'] = array();
}

//    echo "<pre>";
//    echo print_r($_SESSION['shopping_cart']);
//    echo "</pre>";
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways! Come shop with us! Book a trip today!">
    <meta name="og:title" content="Bon Voyage Holidays | Home">
    <meta name="og:description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways!">
    <meta name="og:image" content="https://www.bonvoyageholidays.live/images/logo_300x300.png">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
    <title>Home | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="home">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<a class="login-page-button logout logout-js logout-hide">Log Out</a>
<div id="banner" class="banner-wrapper">
    <img src="./images/logo.png" alt="bon voyage logo"/>
</div>

<div id="main-content-wrapper">
    <h1 class="featured-header">Featured Trips</h1>
    <div id="featured" class="featured-content-wrapper">
        <a href="/ontario-niagara-falls.php" class="featured-trips featured-trip-button" id="niagara">
            <img src="./images/niagara-falls.jpg" alt="niagara falls"/>
            <h3>Niagara-On-The-Lake and Niagara Falls</h3>
            <p>Join as we explore Ontario's wine country. We visit six large-scale wineries in the
                Niagara-On-The-Lake area and four family-owned vineyards across the Bench region.
                After a few days of quiet relaxation, it's time to turn up the fun metre in Niagara Falls.</p>
        </a>

        <a href="/quebec-montreal.php" class="featured-trips featured-trip-button" id="montreal">
            <img src="./images/montreal.jpg" alt="old montreal street"/>
            <h3><br>Montreal and all that Jazz</h3>
            <p>Ready to explore Quebec's metropolis? This tour includes sightseeing in Old Montreal, admission
                to the Montreal Museum of Fine Arts, a sightseeing cruise along the St. Lawrence, and much,
                much more.</p>
        </a>

        <a href="/eastcoast-ns.php" class="featured-trips featured-trip-button" id="novascotia">
            <img src="./images/nova-scotia.jpg" alt="nova scotia lighthouse"/>
            <h3><br>Nova Scotia</h3>
            <p>This getaway shows us the true spirit of Nova Scotia and its people. Come along and see the best of
                this spectacular province! Majestic views along the coast and a visit to Peggy's Cove cap off our
                most scenic trip.</p>
        </a>

        <a href="/ontario-georgian-bay.php" class="featured-trips featured-trip-button" id="georgianbay">
            <img src="./images/georgian-bay.jpg" alt="georgian bay beach"/>
            <h3>Georgian Bay<br>- Nature's Escape</h3>
            <p>Escape to nature with this weeklong trip to one of Ontario's most beautiful regions. Hike through
                the white pine forests, explore caverns along the bedrock beaches, and swim in the beautiful waters
                of Lake Huron.</p>
        </a>
    </div>

    <div id="about" class="about-us-wrapper">
        <h1 class="about-us-header">About Us</h1>
        <div class="about-us-text-wrapper">
            <p>Lindsay Tiedemann and Brendan Tate began the company in 2018, specializing in group travel to
                destinations spanning Ontario, Quebec, and the East Coast. Bon Voyage Holidays operates a full motor
                coach division with over 20 coaches and the most modern fleet in Southwestern Ontario. The tour
                division offers a broad array of multi-day tours geared towards a wide range of travellers.</p>
        </div>
    </div>

    <div id="why-us" class="why-us-wrapper">
        <h2 class="why-us-header">Why travel with Bon Voyage Holidays?</h2>
        <div class="why-us-text-wrapper">
            <p>We are a 100% Canadian-owned-and-operated business. Our commitment to promoting Canadian travel ensures
                that you will experience the best that our wonderful country has to offer. We hope that all our
                travellers consider themselves not only "Canadian Ambassadors", but "Southwestern Ontario Ambassadors"
                as we travel the highways and byways in our quest to see Canada in all its beauty.</p>
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>

    <div id="contact" class="contact-us-wrapper">
        <h1 class="contact-us-header">Contact Us</h1>
        <div class="contact-us-content-wrapper">
            <div class="contact-us-content"><img src="./images/logo.png" alt="map of bon voyage holidays logo"/></div>
            <div class="contact-us-content">
                <p>E-mail: <a href="mailto:help@bonvoyageholidays.live">help@bonvoyageholidays.live</a></p>
                <p>Phone: <a href="tel:1-866-BON-VOYAGE">1-866-BON-VOYAGE</a></p>
                <p>Address: 1001 Fanshawe College Boulevard<br>London, Ontario, Canada</p>
            </div>
            <div class="contact-us-content"><img src="./images/map.png" alt="map of bon voyage holidays location"/></div>
        </div>
    </div>
</div>
<footer></footer>
</body>
</html>