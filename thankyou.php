<?php
session_start();
if (!isset($_SESSION['shopping_cart'])) {
    $_SESSION['shopping_cart'] = array();
}

//    echo "<pre>";
//    echo print_r($_SESSION['shopping_cart']);
//    echo "</pre>";
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thank You | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>

<body class="thank-you">
<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="thank-you-wrapper">
    <h2>Thank you for your order!</h2>
    <h4>Please send your $200 deposit at least 30 days before the beginning of your trip. Your remaining balance is due one week
    before your trip's departure date.</h4>
    <h4>If you have any questions, please don't hesitate to contact us at
        <a href="mailto:help@bonvoyageholidays.live">help@bonvoyageholidays.live</a> or
        <a href="tel:1-866-BON-VOYAGE">1-866-BON-VOYAGE</a>.
    </h4>
    <a id="return_home" class="btn-thank-you">Home</a>
</div>



