<?php
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways! Come shop with us! Book a trip today!">
    <meta name="og:title" content="Bon Voyage Holidays | Home">
    <meta name="og:description"
          content="Bon Voyage Holidays is your one stop shop for holiday getaways!">
    <meta name="og:image" content="https://www.bonvoyageholidays.live/images/logo_300x300.png">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
    <title>Admin | Bon Voyage Holidays</title>
    <link href="../css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="../images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="../js/index.min.js"></script>
    <script defer src="../js/admin.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<?php require_once("../includes/nav.php"); ?>

<form class="add-trip-wrapper">
    <h1>Add new Trips</h1>
    <!-- Province -->
    <h2>Province:</h2>
    <input class="ontariorb" name="province" id="ontario" type="radio" value="Ontario">
    <label for="ontario">Ontario</label><br/>
    <input class="eastcoastrb" name="province" id="eastcoast" type="radio" value="EastCoast">
    <label for="eastcoast">East Coast</label><br/>
    <input class="quebecrb" name="province" id="quebec" type="radio" value="Quebec">
    <label for="quebec">Quebec</label><br/>
    <br/>
    <br/>


    <!-- Dates -->
    <h2>Dates:</h2>
    <label for="date-from">From:</label>
    <br/>
    <input class="date-from" id="date-from" type="date">
    <label for="depart-time">Time:</label>
    <input class="depart-time" id="depart-time" type="time"/>
    <br/>
    <br/>
    <label for="date-to">To:</label>
    <br/>
    <input class="date-to" id="date-to" type="date">
    <br/>
    <br/>

    <div class="Ontario">
        <h2>Location:</h2>
        <input value="GeorgianBay" name="bg" id="georgianbay" type="radio">
        <label for="georgianbay">Georgian Bay</label>
        <br/>
        <input value="NiagaraFalls" name="bg" id="niagarafalls" type="radio">
        <label for="niagarafalls">Niagara Falls</label>
        <br/>
        <input value="Niagara-nol" name="bg" id="niagara-nol" type="radio">
        <label for="niagara-nol">Niagara on the lake</label>
        <br/>
        <input value="Ottawa" name="bg" id="ottawa" type="radio">
        <label for="ottawa">Ottawa</label>
        <br/>
        <input value="Toronto" name="bg" id="toronto" type="radio">
        <label for="toronto">Toronto</label>
        <br/>
        <input value="Toronto-Theatre" name="bg" id="toronto-theatre" type="radio">
        <label for="toronto-theatre">Toronto Theatre</label>
    </div>

    <div class="Quebec">
        <h2>Location:</h2>
        <input value="MontTremblant" name="bg" id="mont-tremblant" type="radio">
        <label for="mont-tremblant">Mont Tremblant</label>
        <br/>
        <input value="Montreal" name="bg" id="montreal" type="radio">
        <label for="montreal">Montreal</label>
        <br/>
        <input value="Quebec-R-R" name="bg" id="qrr" type="radio">
        <label for="qrr">Quebec - Rivierre Du Loupe - Rimouski</label>
        <br/>
        <input value="QuebecCity" name="bg" id="qc" type="radio">
        <label for="qc">Quebec City</label>
    </div>

    <div class="EastCoast">
        <h2>Location:</h2>
        <input value="NewBrunswick" name="bg" id="nb" type="radio">
        <label for="nb">New Brunswick</label>
        <br/>
        <input value="NewBrunswick-NovaScotia" name="bg" id="nb-ns" type="radio">
        <label for="nb-ns">New Brunswick and Nova Scotia</label>
        <br/>
        <input value="NewBrunswick-Quebec" name="bg" id="nb-qc" type="radio">
        <label for="nb-qc">New Brunswick and Quebec</label>
        <br/>
        <input value="NovaScotia" name="bg" id="ns" type="radio">
        <label for="ns">Nova Scotia</label>
    </div>

    <!-- Price -->
    <h2>Price:</h2>
    <label for="price"></label>
    <br/>
    <input class="price" id="price" type="number">
    <br/>
    <br/>


    <!-- Description -->
    <label for="description-field-admin"></label>
    <textarea class="description-field-admin" id="description-field-admin"></textarea>
    <br/>
    <br/>

    <!-- Submit Button -->
    <label for="btn-admin-submit"></label>
    <input type="button" id="btn-admin-submit" value="Submit" class="btn-admin-submit">
</form>
</body>
</html>
