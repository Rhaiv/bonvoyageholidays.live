<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="home">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<a class="login-page-button logout logout-js logout-hide">Log Out</a>
<div class="login-form">
    <div class="login-wrapper">
        <div class="login-input-wrapper">
            <label class="label-style" for="txtUsername">Username</label>
            <input class="txtBox" type="text" name="test_data" autocomplete="username" id="txtUsername"/>
            <label class="label-style" for="txtPassword">Password</label>
            <input class="txtBox" type="password" autocomplete="current-password" id="txtPassword"/>
        </div>
        <div class="login-btn-wrapper">
            <a class="login-page-button login-js">Log In</a>
            <a href="/" class="login-page-button cancel-button-js">Cancel</a>
        </div>
        <div class="sign-up-wrapper">
            <span class="no-account-span">Don't have an account?</span>
            <a href="/signUp" class="login-page-button" name="signUp">Sign Up</a>
        </div>
    </div>
</div>
<?php
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
</body>
</html>