<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quebec City | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-details">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>

<img class="logo" src="images/logo.png" alt="bon voyage travel logo"/>

<div class="description-wrapper">
    <div class="description">


        <img src="images/quebec-description.jpg" alt="quebec city street"/>
        <div class="description-content-wrapper">
            <h1 class="description-alignment">Quebec City</h1>
            <h3 class="description-alignment">Description</h3>
            <p class="description-content">Quebec City is one of the most historic and intriguing cities in North America with its old-world
                atmosphere mixed with the excitement of today’s hustle and bustle. It’s a treasure-trove of activities, attractions, restaurants,
                and delightful places to spend a few minutes or hours. Join us on this specially-crafted itinerary where we show you the best the city has to offer!
                This itinerary spends six nights in historic Old Quebec City, and takes in the historic sites, as well as all the highlights of the coast of the St.
                Lawrence River. Each and every day features incredible sightseeing and dining. If you’ve always dreamed of seeing quintessential Canadian history,
                don’t put it off any longer. Come along and join the fun!
            </p>

            <h3 class="description-alignment">Sample Itinerary</h3>
            <h4 class="description-alignment">Day 1</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>07:30-18:00</td>
                    <td>London-Quebec City</td>
                    <td>Travel by coach bus to Quebec City</td>
                </tr>
                <tr>
                    <td>18:00-18:45</td>
                    <td>Hôtel le Saint-Paul</td>
                    <td>Hotel Check-In</td>
                </tr>
                <tr>
                    <td>19:00-20:30</td>
                    <td>Légende</td>
                    <td>Dinner - Canadian Cuisine</td>
                </tr>

            </table>

            <h4 class="description-alignment">Day 2</h4>

            <table class="table-wrapper">
                <tr>
                    <th>Date/Time</th>
                    <th>Location</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>08:30-09:15</td>
                    <td>Buffet de L'Antiquaire</td>
                    <td>Breaskfast - Canadian Cuisine</td>
                </tr>
                <tr>
                    <td>09:30-13:00</td>
                    <td>La Citadelle de Québec</td>
                    <td>Historical Tour of the Citadel</td>
                </tr>
                <tr>
                    <td>13:00-17:30</td>
                    <td>Old Quebec</td>
                    <td>Free Time in Old Quebec</td>
                </tr>
                <tr>
                    <td>18:00-20:00</td>
                    <td>Bistro sous le fort</td>
                    <td>Dinner - French Cuisine</td>
                </tr>

            </table>
            <a id="Q_QC_1527339600000" class="btn-add-to-cart btn-cart-checkout">Add to Cart</a>

        </div>
    </div>
</div>
</body>
</html>