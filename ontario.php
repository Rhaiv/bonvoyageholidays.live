<?php
session_start();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ontario Trips | Bon Voyage Holidays</title>
    <link href="./css/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="./images/favicon.png" rel="icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script defer src="js/index.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="trip-lists">
<?php require_once("./includes/nav.php"); ?>
<?php require_once("./includes/cart-dropdown.php"); ?>
<a class="login-page-button logout logout-js logout-hide">Log Out</a>
<img src="images/logo.png" alt="bon voyage holidays logo"/>

<div class="table-wrapper">
    <div class="table">
        <h2>Ontario</h2>
        <table>
            <tr>
                <th>Location(s)</th>
                <th>Length of Trip</th>
                <th>Dates</th>
            </tr>
            <tr>
                <td><a href="ontario-georgian-bay">Georgian Bay</a></td>
                <td>7 days</td>
                <td>Monday, May 28 2018 - Sunday, June 3 2018
                    <br>Monday, July 9 2018 - Sunday, July 15 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-niagara-falls">Niagara Falls<a></td>
                <td>4 days</td>
                <td>Tuesday, May 8 2018 - Friday, May 11 2018
                    <br>Tuesday, May 15 2018 - Friday, May 18 2018
                    <br>Tuesday, May 22 2018 - Friday, May 25 2018
                    <br>Tuesday, May 29 2018 - Friday, June 1 2018
                    <br>Tuesday, June 5 2018 - Friday, June 8 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-niagara-falls-nol">Niagara Falls and Niagara-On-The-Lake</a></td>
                <td>7 days</td>
                <td>Sunday, May 6 2018 - Saturday, May 12 2018
                    <br>Sunday, May 27 2018 - Saturday, June 2 2018
                    <br>Monday, June 18 2018 - Sunday, June 24 2018
                    <br>Sunday, July 8 2018 - Saturday, July 14 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-ottawa">Ottawa</a></td>
                <td>5 days</td>
                <td>Sunday, May 13 2018 - Thursday, May 17 2018
                    <br>Sunday, June 10 2018 - Thursday, June 14 2018
                    <br>Sunday, July 8 2018 - Thursday, July 12 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-ottawa">Ottawa</a></td>
                <td>7 days</td>
                <td>Sunday, May 13 2018 - Saturday, May 19 2018
                    <br>Sunday, June 10 2018 - Saturday, June 16 2018
                    <br>Sunday, July 8 2018 - Saturday, July 14 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-toronto-theatre">Toronto - Theatre</a></td>
                <td>1 day</td>
                <td>Wednesday, May 2 2018
                    <br>Saturday, May 5 2018
                    <br>Wednesday, May 9 2018
                    <br>Saturday, May 12 2018
                    <br>Wednesday, May 16 2018
                    <br>Saturday, May 19 2018
                    <br>Wedneday, May 23 2018
                    <br>Saturday, May 26 2018
                    <br>Wednesday, May 30 2018
                </td>
            </tr>
            <tr>
                <td><a href="ontario-toronto">Toronto</a></td>
                <td>4 days</td>
                <td>Monday, May 28 2018 - Thursday, May 31 2018
                    <br>Monday, June 11 2018 - Thursday, June 14 2018
                    <br>Monday, June 25 2018 - Thursday, June 28 2018
                </td>
            </tr>
        </table>
    </div>
</div>

</body>
</html>